var controller;
$(document).ready(function() { 
    var sceneDuration;
    if(isDesktopWidth()) {
        sceneDuration = (($("#pinTarget").offset().top - $("#pin1").offset().top) - $("#pin1").height());
        controller = new ScrollMagic();
        var logoScene = new ScrollScene({ duration: sceneDuration})
                        .setPin("#pin1", {pushFollowers: false})
                        .on("end", slideUpIntro)
                        .addTo(controller);      
    } else {
        sceneDuration = $("#pinTarget").offset().top;
    }
    $("#landing-arrow").click(function() {
        $('html, body').animate({
            scrollTop: sceneDuration
        }, 1600);
    });
});
function slideUpIntro() {
    $(".intro").addClass("slideUp");
}
function isDesktopWidth() { // 
    return $('.desktopIndicator').is(':visible');
}